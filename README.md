# Edytor Partii Szachowej

Projekt został oparty na Pythonie 2, Django 1.8 oraz MySQL 5.7.
Uruchomienie serwera odbywa się poleceniem


```
#!bash

cd docker

```
```
#!bash

docker-compose up

```

a następnie w przeglądarce
```
http://127.0.0.1:8000/szachy/
```
