from django import forms
from chessboard.szachy.models import PARTIE,ZAWODNICY,STANY,RUCHY


class NameForm(forms.Form):
    name = forms.CharField(label=u'Imie', max_length=20,required=False)
    surname = forms.CharField(label=u'Nazwisko',max_length=20,required=False)
    
class ZawodnicyList(forms.Form):
    zawodnik = forms.ChoiceField(label=u'Zawodnik')

    def __init__(self, *args, **kwargs):
        super(ZawodnicyList, self).__init__(*args, **kwargs)
        listaGraczy = ZAWODNICY.objects.all().order_by('ID_ZAWODNIKA')
        self.fields['zawodnik'].choices = [(e.ID_ZAWODNIKA, str(e.ID_ZAWODNIKA)+" "+e.NAZWISKO+" "+e.IMIE) for e in listaGraczy]
        
class ZawodnicyDoubleList(forms.Form):
    zawodnik1 = forms.ChoiceField(label=u'Zawodnik 1')
    zawodnik2 = forms.ChoiceField(label=u'Zawodnik 2')

    def __init__(self, *args, **kwargs):
        super(ZawodnicyDoubleList, self).__init__(*args, **kwargs)
        listaGraczy = ZAWODNICY.objects.all().order_by('ID_ZAWODNIKA')
        self.fields['zawodnik1'].choices = [(e.ID_ZAWODNIKA, str(e.ID_ZAWODNIKA)+" "+e.NAZWISKO+" "+e.IMIE) for e in listaGraczy]
        self.fields['zawodnik2'].choices = [(e.ID_ZAWODNIKA, str(e.ID_ZAWODNIKA)+" "+e.NAZWISKO+" "+e.IMIE) for e in listaGraczy]


class PartieList(forms.Form):
    partia = forms.ChoiceField(label=u'Partia')

    def givePlayer(self, idP ,gracze_list):
        tmp_str = []
        for item in gracze_list:
            if idP == item.ID_ZAWODNIKA:
                    tmp_str.append(item.NAZWISKO+" "+item.IMIE)    
        return tmp_str[0]

    def __init__(self, *args, **kwargs):
        super(PartieList, self).__init__(*args, **kwargs)
        listaPartii = PARTIE.objects.all().order_by('ID_PARTII')
        gracze_list = ZAWODNICY.objects.all().order_by('ID_ZAWODNIKA')
        self.fields['partia'].choices = [(e.ID_PARTII, str(e.ID_PARTII)+" "+self.givePlayer(e.BIALE, gracze_list)+" "+self.givePlayer(e.CZARNE, gracze_list)) for e in listaPartii]
