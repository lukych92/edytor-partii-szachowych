from django.db import models

# Create your models here.
class PARTIE(models.Model):
    ID_PARTII = models.IntegerField(primary_key=True)
    BIALE = models.IntegerField()
    CZARNE = models.IntegerField()
    ZWYCIEZCA = models.CharField(max_length=3)
    
    def __unicode__(self):
        return "ID: "+str(self.ID_PARTII)+" "+str(self.BIALE)+" "+str(self.CZARNE)+" "+self.ZWYCIEZCA
    
    class Meta:
        db_table = "PARTIE"
    
class RUCHY(models.Model):
    ID_PARTII = models.IntegerField(primary_key=False)
    RUCH_Z = models.CharField(max_length=2)
    RUCH_DO = models.CharField(max_length=2)
    NR_RUCHU = models.IntegerField()
    KOMENTARZ = models.CharField(max_length=3)
    FEN = models.CharField(max_length=200)
    
    def __unicode__(self):
        return self.RUCH
    class Meta:
        db_table = "RUCHY"  
        
    def getRuch(self):
        return self.RUCH
    
class STANY(models.Model):
    ID_PARTII = models.IntegerField(primary_key=True)
    POZYCJA = models.CharField(max_length=2)
    BIEREK = models.CharField(max_length=1)
    
    def __unicode__(self):
        return "ID: "+str(self.ID_PARTII)+" "+self.POZYCJA+" "+self.BIEREK
  
    class Meta:
        db_table = "STANY"
        
class ZAWODNICY(models.Model):
    ID_ZAWODNIKA = models.IntegerField(primary_key=True)
    NAZWISKO = models.CharField(max_length=20)
    IMIE = models.CharField(max_length=20)
    
    def __unicode__(self):
        return "ID: "+str(self.ID_ZAWODNIKA)+" "+self.NAZWISKO+" "+self.IMIE
             
    class Meta:
        db_table = "ZAWODNICY"