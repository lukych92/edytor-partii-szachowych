from django.shortcuts import render, redirect
from django.http import HttpResponse,HttpResponseRedirect
from django.template import Context, loader
from django.db.models import Max
from django.views.decorators.csrf import csrf_exempt
from chessboard.szachy.forms import NameForm, ZawodnicyList, PartieList, ZawodnicyDoubleList
from chessboard.szachy.models import PARTIE,ZAWODNICY,STANY,RUCHY

import json
from tempfile import mkstemp
from shutil import move
from os import remove, close

def replace(file_path, pattern, subst):
    #Create temp file
    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                new_file.write(line.replace(pattern, subst))
    close(fh)
    #Remove original file
    remove(file_path)
    #Move new file
    move(abs_path, file_path)

def giveName(tab,gracze_list):
    tmp_str = []
    for i in range(len(tab)):
        for item in gracze_list:
            if tab[i] == item.ID_ZAWODNIKA:
                tmp_str.append(item.NAZWISKO+" "+item.IMIE)
                
    return tmp_str

def givePlayer(idP ,gracze_list):
    tmp_str = []
    for item in gracze_list:
        if idP == item.ID_ZAWODNIKA:
                tmp_str.append(item.NAZWISKO+" "+item.IMIE)    
    return tmp_str[0]

@csrf_exempt
def index(request,idPartii):
    lista =  ZawodnicyDoubleList()
    max_id_part = PARTIE.objects.all().aggregate(Max('ID_PARTII'))
    latest_wyniki_list = PARTIE.objects.all().order_by('ID_PARTII')
    max_id_part = max_id_part['ID_PARTII__max']
    partie = PartieList()
    partia = PARTIE.objects.filter(ID_PARTII=idPartii)
    gracze_list = ZAWODNICY.objects.all().order_by('ID_ZAWODNIKA')    
    move_from = ""
    move_to = ""
    fenTab = ""
    biale = []
    czarne = []
    idp = idPartii
    biale_str = ""
    czarne_str = ""
    fen = ""
    
    if 'partia' in request.POST:
        idp = request.POST['partia']
    else: idp = idPartii
    max_id_ruch = RUCHY.objects.filter(ID_PARTII=idp).aggregate(Max('NR_RUCHU'))
    max_id_ruch = max_id_ruch['NR_RUCHU__max']  
    
    if max_id_ruch == None:
        max_id_ruch=0
    
    if max_id_part == None:
        max_id_part=1
    
    if max_id_ruch==0:
        lastFEN="rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
 #       tmplastFEN = lastFEN
    else:
        lastFEN = RUCHY.objects.filter(ID_PARTII=idp).values('FEN')[max_id_ruch-1]
        lastFEN = lastFEN['FEN']
    
    winner = "brak"

                  
    if request.method == 'POST':
        if 'zawodnik1' and 'zawodnik2' in request.POST:
            biale = request.POST['zawodnik1']
            czarne = request.POST['zawodnik2']
            partia = PARTIE(ID_PARTII=max_id_part+1,BIALE=biale,CZARNE=czarne,ZWYCIEZCA="*")
            partia.save()
        if 'mvfrm' and 'mvto' in request.POST:
            move_from = json.loads(request.POST['mvfrm'])
            move_to = json.loads(request.POST['mvto'])
            fenTab = json.loads(request.POST['fn'])
            winner = json.loads(request.POST['win'])
            partia = PARTIE.objects.filter(ID_PARTII=idp)
            if len(winner)>0:
                partia.update(ZWYCIEZCA=winner[0])
            for i in range(0,len(move_from)):
                RUCHY.objects.update_or_create(ID_PARTII=idp,
                                               RUCH_Z=move_from[i],RUCH_DO=move_to[i],NR_RUCHU=max_id_ruch+i+1,FEN=fenTab[i])

         
    granaPartia = PARTIE.objects.filter(ID_PARTII=idp)
    for i in granaPartia:
        biale_str = givePlayer(i.BIALE, gracze_list)
        czarne_str = givePlayer(i.CZARNE, gracze_list)
    
    latest_ruchy_list = RUCHY.objects.filter(ID_PARTII=idp).order_by('NR_RUCHU')
    partia = [idp,biale_str,czarne_str]
    return render(request, 'index.html',
                  {
                   'latest_ruchy_list': latest_ruchy_list,
                  'list':lista,
                  'partia':partia,
                  'partie':partie,
                  'idpartii':idp,
                  'lastFEN':lastFEN,
                  'max':max_id_ruch,
                  'test':winner
                   })

def start(request):
    return render(request, 'start.html',{})


def gracze(request):
    latest_gracze_list = ZAWODNICY.objects.all().order_by('ID_ZAWODNIKA')
    max_id_zaw = ZAWODNICY.objects.all().aggregate(Max('ID_ZAWODNIKA'))
    max_id_zaw = max_id_zaw['ID_ZAWODNIKA__max']
    list =  ZawodnicyList()
    
    if max_id_zaw == None:
        max_id_zaw = 0
    
    idPartiiDoUsuniecia = []
    
    if request.method == 'POST':
        form = NameForm(request.POST)
        if 'name' and 'surname' in request.POST:#dodawanie zawodnika
            name = request.POST['name']
            surname = request.POST['surname']
            zawodnik = ZAWODNICY(ID_ZAWODNIKA=max_id_zaw+1,NAZWISKO=surname,IMIE=name)
            zawodnik.save()
        if 'zawodnik' in request.POST:#usuwanie zawodnika
            idPartiiDoUsuniecia = []
            idz = request.POST['zawodnik']
            zawodnik = ZAWODNICY(ID_ZAWODNIKA=idz)
            zawodnik.delete()
            usuwanePartie = PARTIE.objects.filter(BIALE=idz)
            for i in usuwanePartie:
                idPartiiDoUsuniecia.append(i.ID_PARTII)
            usuwanePartie.delete()
            usuwanePartie = PARTIE.objects.filter(CZARNE=idz)
            for i in usuwanePartie:
                idPartiiDoUsuniecia.append(i.ID_PARTII)
            usuwanePartie.delete()
            for i in idPartiiDoUsuniecia:
                partieDoUsuniecia = RUCHY.objects.filter(ID_PARTII=i)
                partieDoUsuniecia.delete()
    else:
        form = NameForm
    return render(request, 'gracze.html', 
                  {'latest_gracze_list': latest_gracze_list,
                   "form":form,
                   "list":list,
                   "test":idPartiiDoUsuniecia
                   })
@csrf_exempt
def wyniki(request):
    latest_wyniki_list = PARTIE.objects.all().order_by('ID_PARTII')
    gracze_list = ZAWODNICY.objects.all().order_by('ID_ZAWODNIKA')
    form = PartieList
    biale = []
    czarne = []
    
    
    if request.method == 'POST':
        if 'partia' in request.POST:
            prt = PARTIE(ID_PARTII=request.POST['partia'])
            prt.delete()
            rch = RUCHY(ID_PARTII=request.POST['partia'])
            rch.delete()
    
    for idp in latest_wyniki_list:
        biale.append(idp.BIALE)
        czarne.append(idp.CZARNE)

    biale_str = giveName(biale, gracze_list)
    czarne_str = giveName(czarne, gracze_list)

    latest_wyniki_list = zip(latest_wyniki_list,biale_str,czarne_str)
    return render(request, 'wyniki.html', 
                  {
                   'latest_wyniki_list': latest_wyniki_list,
                   'PartieList':form
                   })

def archiwum(request, idPartii,nrRuchu):
    latest_partie_list= PARTIE.objects.all().order_by('ID_PARTII')
    partie = PartieList()
    idp=idPartii
    nrr=int(nrRuchu)
    if '+' in request.POST:
        nrr+=1
    elif '-' in request.POST:
        nrr-=1
        
    if request.method == 'POST':
        if 'partia' in request.POST:
            idp = request.POST['partia']

    else:
        idp = idPartii
        
    test = request.POST
    max_ruch =  RUCHY.objects.filter(ID_PARTII=idp).aggregate(Max('NR_RUCHU'))
    max_ruch = max_ruch['NR_RUCHU__max']

    latest_ruchy_list = RUCHY.objects.filter(ID_PARTII=idp).order_by('NR_RUCHU')
    return render(request, 'archiwum.html', 
                  {'latest_ruchy_list': latest_ruchy_list,
                   'latest_partie_list':latest_partie_list,
                   'partie':partie,
                   'idpartii':idp,
                   'test':test,
                   'nrr':max_ruch
                   })